/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java_tests;


/**
 *
 * @author dwm77
 */
public class Puzzle3  extends junit.framework.TestCase {
    int number;
    volatile Exception exception;
    volatile Error error;
    
    public void test() throws InterruptedException {
        number = 0;
        Thread t = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    assertEquals(2, number);
                }catch(Exception e) {
                    exception = e;
                }catch(Error err) {
                    error = err;
                }
            }
        });
        number = 1;
        t.start();
        number++;
        t.join();
    }
    public void tearDown() throws Exception {
        if(error != null) {
            throw error;
        }
        if(exception != null) {
            throw exception;
        }
    }
}
