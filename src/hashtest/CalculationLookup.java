/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hashtest;

/**
 *
 * @author davidm
 */
public class CalculationLookup {
    private float expense;
    private int monthNumber;
    private float income;
    private int calcId;
    private long seconds;

    void setExpense(float f) {
        expense = f;
    }



    void setMonthNumber(int i) {
        monthNumber = i;
    }

    void setIncome(float f) {
        income = f;
    }
    Float getIncome() {
        return income;
    }

    void setCalculationId(int i) {
        calcId = i;
    }

    void setTimestamp(long time) {
        seconds = time;
    }
    
}
