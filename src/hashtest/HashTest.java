/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hashtest;

import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;


/**
 *
 * @author davidm
 */
public class HashTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Date now = new Date();
        CalculationLookup calcLookup = new CalculationLookup();
        calcLookup.setCalculationId(9);
        calcLookup.setMonthNumber(3);
        calcLookup.setIncome(45000f);
        calcLookup.setExpense(34000f);
        calcLookup.setTimestamp(now.getTime());
        String[] values = new String[2];
        values[0] = "4";
        values[1] = "8";
        HashMap lookups = new HashMap<String, CalculationLookup>();
       
        
        try {
            String md5sum = JavaMD5Sum.computeSum(concat(values));
            System.out.print(">>>>>>>>>>>>>>>>> " + md5sum + "\r\n");
            
            lookups.put(md5sum, calcLookup);
            
            
            CalculationLookup lookup = (CalculationLookup) lookups.get(md5sum);
            System.out.println(">>>>>>>>>>>>>>>>>>>>> "+lookup.getIncome()+"\r\n");
            
            
            String[] values2 = new String[2];
            values2[0] = "4";
            values2[1] = "8";
            String md5sum2 = JavaMD5Sum.computeSum(concat(values2));
            System.out.print(">>>>>>>>>>>>>>>>> " + md5sum2 + "\r\n");
        } catch (NoSuchAlgorithmException ex) {
            System.out.println(ex.getMessage());
        }

    }
    
    private static String concat(String[] values) {
        String returner = "";
        for(int i = 0; i<values.length; i++) {
            returner.concat(values[i]);
        }
        return returner;
    }
    
    
}
