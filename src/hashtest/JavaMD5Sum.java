/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hashtest;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author davidm
 */
public class JavaMD5Sum {
	
	private static final String MD5_ALGORITHM = "MD5";

	/**
	 * Uses Java to compute the MD5 sum of a given input String.
	 * @param input
	 * @return
	 */
	public static final String computeSum(String input)
		throws NoSuchAlgorithmException {

		if (input == null) {
		   throw new IllegalArgumentException("Input cannot be null!");
		}

		StringBuffer sbuf = new StringBuffer();
		MessageDigest md = MessageDigest.getInstance(MD5_ALGORITHM);
		byte [] raw = md.digest(input.getBytes());
		
		for (int i = 0; i < raw.length; i++) {
			int c = (int) raw[i];
			if (c < 0) {
				c = (Math.abs(c) - 1) ^ 255;
			}
			String block = toHex(c >>> 4) + toHex(c & 15);
			sbuf.append(block);
		}
		
		return sbuf.toString();
		
	}

	private static final String toHex(int s) {
		if (s < 10) {
		   return new StringBuffer().
                                append((char)('0' + s)).
                                toString();
		} else {
		   return new StringBuffer().
                                append((char)('A' + (s - 10))).
                                toString();
		}
	}

}