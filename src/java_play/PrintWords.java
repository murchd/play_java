package java_play;


public class PrintWords {
	static int mask = 0; //kind of bit mask
	static int pointer = 0; //position pointer
	public static void main(String[] args) {
		int number = 4562; // the number in question
		String strNum = String.valueOf(number);
		mask = 1<<strNum.length()-1;
		StringBuffer sb = new StringBuffer();
		switch(mask) {
		case 16://10,000
			sb.append(getNumberTens(Integer.valueOf(strNum.substring(pointer, pointer+1))));
		case 8://1,000
			sb.append(getNumberOnes(Integer.valueOf(strNum.substring(pointer, pointer+1))));
			sb.append("Thousand, ");
		case 4://100
			sb.append(getNumberOnes(Integer.valueOf(strNum.substring(pointer, pointer+1))));
			sb.append("Hundred, and ");
		case 2://10
		case 1://1
			sb.append(getNumberTens(Integer.valueOf(strNum.substring(pointer, pointer+1))));
			sb.append(getNumberOnes(Integer.valueOf(strNum.substring(pointer, pointer+1))));
		}
		System.out.println(sb.toString());
	}
	public static String getNumberOnes(int num) {
		String number = "";
		switch(num) {
		case 1:
			number = "One";
			break;
		case 2:
			number = "Two";
			break;
		case 3:
			number = "Three";
			break;
		case 4:
			number = "Four";
			break;
		case 5:
			number = "Five";
			break;
		case 6:
			number = "Six";
			break;
		case 7:
			number = "Seven";
			break;
		case 8:
			number = "Eight";
			break;
		case 9:
			number = "Nine";
			break;
		}
		pointer++;
		return number + " ";
	}
	public static String getNumberTens(int num) {
		String number = "";
		switch(num) {
		case 1:
			number = "Ten";
			break;
		case 2:
			number = "Twenty";
			break;
		case 3:
			number = "Thirty";
			break;
		case 4:
			number = "Forty";
			break;
		case 5:
			number = "Fifty";
			break;
		case 6:
			number = "Sixty";
			break;
		case 7:
			number = "Seventy";
			break;
		case 8:
			number = "Eighty";
			break;
		case 9:
			number = "Ninety";
			break;
		}
		pointer++;
		return number + "-";
	}
}
