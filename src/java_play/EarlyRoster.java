/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java_play;

import java.util.Random;

/**
 *
 * @author davidm
 */
class EarlyRoster {

    public static void main(String args[]) {
        new EarlyRoster().pickAtRandom();
    }

    public void pickAtRandom() {
        String[] teamMembers = new String[]{"David", "Kelvin", "Simon", "Anna", "Gordon", "Tim"};
        Random rand = new Random();
        String name = teamMembers[rand.nextInt(teamMembers.length)];
        System.out.println("The winner is " + name);
    }
}