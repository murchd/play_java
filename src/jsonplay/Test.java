package jsonplay;

import java.util.HashMap;

import org.json.JSONObject;

public class Test {
	public enum Day {MON,TUE,WED};
 public static void main(String[] args) {
	 HashMap<Day,Task> h = new HashMap<Day, Task>();
	 JSONObject j = new JSONObject();
	 j.put("name", "David");
	 
	 
	 Task t = new Task();
	 t.setEventName("Census");
	 t.setDate("Feb");
	 
	 Task t2 = new Task();
	 t2.setEventName("Car");
	 t2.setDate("Mar");
	 
	 
	 
	 h.put(Day.MON,t);
	 h.put(Day.WED, t2);
	 
	 j.put("data", h);
	 
	 //System.out.println(j.toString());
	 
	 
	 JSONObject newj = new JSONObject(j.toString());
	 JSONObject data = newj.getJSONObject("data");
	 JSONObject monday = data.getJSONObject(Day.MON.name());
	 System.out.println(monday);
	 
 }
}
