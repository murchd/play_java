/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package play_java;

/**
 *
 * @author dwm77
 */
public class Sudoku {
    private static final int boardSize = 9;
    private static int[][] matrix = new int[boardSize][boardSize]; 
    
    public static void main(String[] args) {
        buildGrid();
        printGrid();
        
    }
    
    private static void buildGrid() {
        for(int x = 0; x<boardSize;x++) {
            for(int y = 0; y<boardSize;y++) {
                matrix[x][y] = 0;
            }
        }
    }
    
    private static void printGrid() {
        for(int x = 0; x<boardSize;x++) {
            System.out.println("\r\n");
            for(int y = 0; y<boardSize;y++) {
                System.out.print(matrix[x][y]);
            }
        }
    }
}
