/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package play_java;

import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author dwm77
 */
public class Gap {
    private static final int GAP_SIZE = 10 * 1024; // 10 kilobytes
    public static void main(String[] args) throws IOException {
        File tmp = File.createTempFile("gap", ".txt");
        FileOutputStream out = new FileOutputStream(tmp);
        out.write(1);
        out.write(new byte[GAP_SIZE]);
        out.write(2);
        out.close();
        InputStream in = new BufferedInputStream(new FileInputStream(tmp));
        int first = in.read();
        skipFully(in, GAP_SIZE);
        int last = in.read();
        System.out.println(first + last);
    }
    
    private static void skipFully(InputStream is, long nBytes) throws IOException {
        long remaining = nBytes;
        while(remaining != 0) {
            long skipped = is.skip(remaining);
            if(skipped == 0) {
                throw new EOFException();
            }
            remaining -= skipped;
        }
    }
}
