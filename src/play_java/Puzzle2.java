/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package play_java;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author dwm77
 */
public class Puzzle2 {
    private static final String[] URL_NAMES = {
                                                "http://javapuzzlers.com",
                                                "http://apache2-snort.skybar.dreamhost.com",
                                                "http://www.google.com",
                                                "http://javapuzzlers.com",
                                                "http://findbugs.sourceforge.net",
                                                "http://www.cs.umd.edu"
                                                };
    public static void main(String args[]) throws URISyntaxException {
        Set<URI> favorites = new HashSet<URI>();
        for (String urlname: URL_NAMES) {
            favorites.add(new URI(urlname));
        }
        System.out.println(favorites.size());
        
    }
}
