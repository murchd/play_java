/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package play_java;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author dwm77
 */
public class Puzzle1 {
    public static void main(String args[]) {
        Set<Short> s = new HashSet<Short>();
        for (short i = 0; i < 100; i++) {
            s.add(i);
            s.remove((short)(i - 1));
        }
        System.out.println(s.size());
    }
}
