/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package play_java;

/**
 *
 * @author dwm77
 */
public class Elvis {
    
    public static final Elvis ELVIS = new Elvis();
    private Elvis() {}
    
    private static final boolean LIVING = true;
    
    private final boolean alive = LIVING;
    
    public final boolean lives() { return alive; }
    
    public static void main(String[] args) {
        System.out.println(ELVIS.lives() ? "Hound Dog" : "Heartbreak Hotel");
    }
    
}
