/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package play_java;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author davidm
 */
public class ArraySort {
    public static void main(String args[]) {
        new ArraySort().run();
    }
    public void run() {
        HashMap states = new HashMap<String,HashMap>();
        HashMap itemSet = new HashMap<String,Item>();
        
        List<Item> items = new ArrayList<Item>();
        items.add(new Item("Canterbury"));
        items.add(new Item("Otago"));
        
        Iterator<Item> itemIterator = items.iterator();
        while(itemIterator.hasNext()) {
            Item item = itemIterator.next();
            states.put(item.state,itemSet.put(item.state, item));
        }
        System.out.println("Done.");
    }
    
    class Item {
        public String state;
        Item(String state) {
            this.state = state;
        }
    }
}
