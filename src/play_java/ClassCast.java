/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package play_java;

import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author dwm77
 */
public class ClassCast {

    public static void main(String[] args) {

        HashMap m = new HashMap();

        m.put("First", new MyClass());
        m.put("Second", new MyClass());
        m.put("Third", new MyClass());

        Iterator<MyClass> iter = m.values().iterator();

        while (iter.hasNext()) {
            iter.next().SaySomething(); // Throws ClassCastException
        }
    }
}

class MyClass {

    void SaySomething() {
        System.out.println("Something");
    }
}
