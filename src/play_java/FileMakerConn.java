/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package play_java;

/**
 *
 * @author dwm77
 */
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

class FileMakerConn {

    public static void main(String args[]) {
        Connection con = null;
        String connectionURL = "jdbc:filemaker://192.168.2.10:2399/hl_test?user=Admin&password=Pitlord253";
// Change the connection string according to your db, ip, username and password

        try {

            // Load the Driver class.
            Class.forName("com.filemaker.jdbc.Driver");
            // If you are using any other database then load the right driver here.

            //Create the connection using the static getConnection method
            con = DriverManager.getConnection(connectionURL);

            //Create a Statement class to execute the SQL statement
            Statement stmt = con.createStatement();

            //Execute the SQL statement and get the results in a Resultset
            ResultSet rs = stmt.executeQuery("select * from projects");

            // Iterate through the ResultSet, displaying two values
            // for each row using the getString method

            while (rs.next()) {
                System.out.println("Name= " + rs.getString("name") + " username= " + rs.getString("username"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the connection
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(FileMakerConn.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
