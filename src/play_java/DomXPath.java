/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package play_java;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactoryConfigurationException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.w3c.tidy.Tidy;


/**
 *
 * @author davidm
 */
public class DomXPath {

    public static void main(String[] args) throws XPathExpressionException, ParserConfigurationException, XPathFactoryConfigurationException, FileNotFoundException, IOException {

            Tidy tidy = new Tidy();
            tidy.setXHTML(true);
            InputStream is = new FileInputStream("/home/davidm/output.html");
            OutputStream os = new FileOutputStream("/home/davidm/cleaned.html");
            tidy.parse(is, os);
            //new ByteArrayOutputStream();
            
            InputStream in = new FileInputStream("/home/davidm/cleaned.html");
            Document doc = (Document) Jsoup.parse(in, "utf-8", "");
            Elements ele = doc.select(".mpUsageHdr");
            System.out.println(ele.text());
            System.out.println(doc.select(".mpUsage").attr("href"));


    }
}
